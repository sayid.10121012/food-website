const apikey = "43493163bf0d4752814b19fa0dcdf643";

const base_url = "https://api.spoonacular.com";

Recipes.style.display = "block";
// showing recipes
//

function showRecipes() {
  r_menu.classList.add("selected");

  m_menu.classList.remove("selected");

  Menu.style.display = "none";
  Foods.style.display = "none";
  Recipes.style.display = "block";
}
function showMenu() {
  m_menu.classList.add("selected");

  r_menu.classList.remove("selected");

  Menu.style.display = "block";
  Foods.style.display = "none";
  Recipes.style.display = "none";
}

// to prevent keyboard spam...
let before = "";

SearchMenu.addEventListener("keypress", function (event) {
  if (event.key == "Enter") {
    if (SearchMenu.value != before) {
      before = SearchMenu.value;
      search_values.innerHTML = "";

      $.ajax({
        type: "GET",
        data: {
          query: before,
          apiKey: apikey,
        },
        url: base_url + "/food/menuItems/search",
        success: function (data) {
          const menuItems = data.menuItems;
          console.log(menuItems);
          var i = 0;
          menuItems.forEach(function (item) {
            i = i + 1;
            const el = `<div class="column">
                <div class="box">
                  <h2 id="title">${item.title}</h2>
                  <img src="${item.image}" width="100" height="100" />
                  <div class="row">
                    <table>
                      <tr>
                        <td>Restaurant</td>
                        <td>:</td>
                        <td>${item.restaurantChain}</td>
                      </tr>
                      <tr>
                        <td>Serving Size</td>
                        <td>:</td>
                        <td>${item.readableServingSize ?? "-"}</td>
                      </tr>
                    </table>
                    <a class="more" href="#Menu#${
                      item.id
                    }" onClick="showMenuDetail(${item.id})">
                      See Details...
                    </a>
                  </div>
                </div>
            </div>`;

            const ele = document.createElement("div");
            ele.innerHTML = el;
            search_values.appendChild(ele);
          });
        },
      });
    }
  }
});

function showMenuDetail(id) {
  var stats = document.getElementById("detail_menu").style.display;

  if (stats == "none") {
    document.getElementById("detail_menu").style.display = "inline-block";
    $.ajax({
      type: "GET",
      data: {
        apiKey: apikey,
      },
      url: base_url + "/food/menuItems/" + id,
      success: function (data) {
        const el = `<table id="table_pop" style="width: 100%">
          <tr>
            <td colspan="100%">${data.title}</td>
          </tr>
          <tr>
            <td colspan="100%">
              <center>
                <img src="${data.image}" width="100" height="100" />
              </center>
            </td>
          </tr>
          <tr>
            <td>Restaurant</td>
            <td>:</td>
            <td>${data.restaurantChain ?? "-"}</td>
          </tr>

          <tr>
            <td>Serving Size</td>
            <td>:</td>
            <td>${data.servingSize ?? "-"}</td>
          </tr>
          <tr>
            <td>Price</td>
            <td>:</td>
            <td>${data.price ?? "Unknown"}</td>
          </tr>
          <tr>
            <td colspan="100%">Nutrition</td>
          </tr>
          <tr>
            <td>Calories</td>
            <td>:</td>
            <td>${data.nutrition.calories ?? "Unknown"}</td>
          </tr>
          <tr>
            <td>Fat</td>
            <td>:</td>
            <td>${data.nutrition.fat ?? "Unknown"}</td>
          </tr>
          <tr>
            <td>Protein</td>
            <td>:</td>
            <td>${data.nutrition.protein ?? "Unknown"}</td>
          </tr>
          <tr>
            <td>Carbs</td>
            <td>:</td>
            <td>${data.nutrition.carbs ?? "Unknown"}</td>
          </tr>
        </table>`;
        const ele = document.createElement("div");
        ele.innerHTML = el;
        document.getElementById("detail_menu").appendChild(ele);
      },
    });
  } else {
    document.getElementById("detail_menu").style.display = "none";
  }
}

function closeMenuButton(type) {
  if (type == "Menu") {
    document.getElementById("detail_menu").style.display = "none";
    document.getElementById("table_pop").remove();
  }
}
